#ifndef KARGER_H
#define KARGER_H
#include "grafo.h"
#include <vector>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */


using namespace std;

template <typename N,typename A>
class Karger
{
    public:
        typedef Grafo<N,A> G;
        typedef vector<int> P;

        Karger() {}
        virtual ~Karger() {}

        void run(G &g,int iteraciones)
        {
            g.actualizarPosNodo();
            P inicios(g.aristas.size(),0);
            P fines(g.aristas.size(),0);
            for (unsigned int i=0;i<g.aristas.size();i++)
            {
                inicios[i]=(g.aristas[i]->nodos[0])->pos;
                fines[i]=(g.aristas[i]->nodos[1])->pos;

            }
            P r;
            P s = corteMinimo(g,inicios,fines);

            for (int i=1;i<iteraciones;i++)
            {
                r=corteMinimo(g,inicios,fines);
                if (r.size()<s.size())
                    s=r;
            }
            //muestra las aristas de corte
            for (unsigned int i=0;i<s.size();i++)
                cout <<(g.aristas[s[i]]->nodos[0])->dato<<" - "<< (g.aristas[s[i]]->nodos[1])->dato <<endl;
            cout<<s.size()<<" cortes "<<endl; //imprimimos el numero de cortes, peros s tiene las posiciones de las ariatss del minimo corte
        }

        P corteMinimo(G &g,P inicios, P fines)
        {
            P nodosPos(g.nodos.size(),0);
            P aristasPos(g.aristas.size(),0); //todas las aristas activas
            for (unsigned int i=0;i<g.nodos.size();i++) nodosPos[i]=i; //posicion de los nodos
            for (unsigned int i=0;i<g.aristas.size();i++) aristasPos[i]=i;

            unsigned int iSecret;
            srand (time(NULL));
            int cantidad=nodosPos.size();

            while (cantidad>2)
            {
                iSecret = rand() % (aristasPos.size()-1);
                mergeNodos(iSecret,nodosPos,aristasPos,inicios,fines);
                deleteLoops(inicios,fines,aristasPos);
                cantidad--;
            }
            return aristasPos;
        }
    protected:
    private:
        void mergeNodos(int i,P &nodoPos,P &arisPos,P &arisIni,P &arisFin)
        {
            int a=arisIni[i];
            int b=arisFin[i];
            if (b<a) swap(a,b); //ahora a es siempre el menor
            arisPos.erase(arisPos.begin()+i);
            arisIni.erase(arisIni.begin()+i);
            arisFin.erase(arisFin.begin()+i);

            for (unsigned int j=0;j<nodoPos.size();j++) //actualizar nodos
                if (nodoPos[j]==b) nodoPos[j]=a;
            for (unsigned int j=0;j<arisIni.size();j++) //actualizar aristas
            {
                if (arisIni[j]==b) arisIni[j]=a;
                if (arisFin[j]==b) arisFin[j]=a;
            }
        }

        void deleteLoops(P &arisIni,P &arisFin,P &arisPos)
        {
            unsigned int i=0;
            while (i<arisIni.size())
            {
                if (arisIni[i]==arisFin[i])
               {
                    arisPos.erase(arisPos.begin()+i);
                    arisIni.erase(arisIni.begin()+i);
                    arisFin.erase(arisFin.begin()+i);
               }
               else i++;
            }
        }

};

#endif // KARGER_H
