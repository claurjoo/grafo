#ifndef BINARYHEAP_H
#define BINARYHEAP_H
#include <vector>

using namespace std;

class BinaryHeap
{
    public:
        vector<int> posiciones;
        vector<int> pesos;

        BinaryHeap() {}
        virtual ~BinaryHeap() {}

        void insertar (int a,int p)
        {
            posiciones.push_back(a);
            pesos.push_back(p);
            heapifyUp(posiciones.size()-1);
        }

        void heapifyUp(int hijo)
        {
            if (hijo==0) return;
            int padre = (hijo-1)/2;
            if (pesos[padre]>pesos[hijo])
            {
                swap(pesos[padre],pesos[hijo]);
                swap(posiciones[padre],posiciones[hijo]);
                heapifyUp(padre);
            }
        }

        int findMin ()
        {
            if (posiciones.size()>0) return posiciones.front();
            return -1;
        }

        void deleteMin()
        {
            swap(pesos.front(),pesos.back());
            swap(posiciones.front(),posiciones.back());
            pesos.pop_back();
            posiciones.pop_back();
            heapifyDown(0);
        }

        void heapifyDown(int padre)
        {
            unsigned int hijo1= (padre*2)+1;
            unsigned int hijo2= (padre*2)+2;
            unsigned int hijo;
            if (hijo1>=pesos.size()) return; //no tiene hijos
            if (hijo2>=pesos.size())
            {
                hijo=hijo1; //solo 1 hijo
            }else
            {
                if ( pesos[hijo1]<=pesos[hijo2])
                    hijo=hijo1;
                else
                    hijo=hijo2;
            }
            if (pesos[hijo]<pesos[padre])
            {
                swap(pesos[padre],pesos[hijo]);
                swap(posiciones[padre],posiciones[hijo]);
                heapifyDown(hijo);
            }
        }

        void actualizarPeso(int a, int p)
        {
            unsigned int i;
            for (i=0;posiciones[i]!=a;i++){}
            pesos[i]=p;
            heapifyUp(i);
        }

    protected:
    private:
};

#endif // BINARYHEAP_H
