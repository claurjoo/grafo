#ifndef NODO_H
#define NODO_H
#include "grafo.h"
#include <vector>

using namespace std;

template <typename G>
class Nodo
{
    public:
        typedef typename G::TipoNodo N;
        typedef typename G::aristaG A;

        N dato;
        vector<A*> aristas;
        int pos;

        Nodo(N a) {dato=a;}

        virtual ~Nodo() {}

        bool comprobar(N a) {return dato==a;}

        void insertar_arista(A *a) {aristas.push_back(a);}

        void limpiar(){aristas.clear();}

    protected:
    private:
};

#endif // NODO_H
