#ifndef DISKTRA_H
#define DISKTRA_H
#include "grafo.h"
#include "BinaryHeap.h"
#include <vector>

using namespace std;

template <typename N,typename A>
class Disktra
{
    public:
        typedef Grafo<N,A> G;

        vector<int> distancia,previo;
        BinaryHeap lista;

        Disktra() {}
        virtual ~Disktra() {}

        void generarArbol(G &g,N inicio) //para un arbol estatico
        {
            g.actualizarPosNodo(); //todos los nodos ahora tienen la posicion q ocupan en el grafo
            int cantNodos=g.nodos.size();
            int pos=g.posNodo(inicio); //buscar la posicion del nodo inicial
            if (pos==-1) return;
            //inicializar
            vector<bool> visitados(cantNodos,1); //todos como no visitados (disponibles)
            distancia.resize(cantNodos,1000000);
            previo.resize(cantNodos,-1);
            //vector<int> lista;
            lista.insertar(pos,0);
            distancia[pos]=0;
            int x,y,nodo,peso;

            while (lista.posiciones.size()>0)
            {
                nodo=lista.findMin();
                lista.deleteMin();

                for (unsigned int i=0;i<g.nodos[nodo]->aristas.size();i++) //todas las aristas del nodo
                {
                    x=g.posNodo2(nodo,i,peso); //pos del 2do nodo, enlasado al 1er nodo
                    y=distancia[nodo]+peso;
                    if (visitados[x])
                    {
                        visitados[x]=0;
                        distancia[x]=y;
                        previo[x]=nodo;
                        lista.insertar(x,y);
                    }else
                    {
                        if (y<distancia[x])
                        {
                            distancia[x]=y;
                            previo[x]=nodo;
                            lista.actualizarPeso(x,y);
                        }
                    }
                }
            }
        }

        void buscar(N a,G &g)
        {
           int pos=g.posNodo(a);
           cout<<"distancia a "<<a<<": "<<distancia[pos]<<endl;
        }
    protected:
    private:
        int minimo(vector<int> &lista)
        {
            int t=0;
            for (unsigned int i=1;i<lista.size();i++)
            {
                if (distancia[lista[i]]<distancia[lista[t]])
                    t=i;
            }
            return t;
        }

};

#endif // DISKTRA_H
