#ifndef GRAFO_H
#define GRAFO_H
#include "nodo.h"
#include "arista.h"
#include <vector>


using namespace std;

template <typename G>
class Nodo;

template <typename G>
class Arista;

template <typename N,typename A>
class Grafo
{
    public:
        typedef Grafo<N,A> self;
        typedef N TipoNodo;
        typedef A TipoArista;
        typedef Nodo<self> nodoG;
        typedef Arista<self> aristaG;

        vector<nodoG*> nodos;
        vector<aristaG*> aristas;

        Grafo() {}
        virtual ~Grafo() {
            unsigned int i;
            for (i=0 ; i<nodos.size(); i++) nodos[i]->limpiar();
            for (i=0 ; i<aristas.size(); i++) aristas[i]->limpiar();

            cout<<nodos.size()<<" nodos - "<<aristas.size()<<" aristas"<<endl;
            for (i=0 ; i<nodos.size(); i++)
                delete nodos[i];
            for (i=0 ; i<aristas.size(); i++)
                delete aristas[i];
        }

        bool insertar(TipoNodo a, TipoNodo b,TipoArista c)
        {
            if (a==b) return false;
            nodoG *p, *q;
            aristaG *r;
            if (a>b) swap(a,b);

            if (crear_nodo(a,p)) insertionSortNodo(p);
            if (crear_nodo(b,q)) insertionSortNodo(q);
            if ( buscar_arista(p,q))return false; //si ya existe la arista, termina

            r = new aristaG(p, q, c);
            aristas.push_back(r);
            p->insertar_arista(r);
            q->insertar_arista(r);
            return true;
        }

        bool crear_nodo(TipoNodo a,nodoG*&p)
        {
            if (buscar_nodo(a,p))
                return false;
            p = new nodoG(a);
            return true;
        }

        void mostrar()
        {
            cout<<"nodos"<<endl;
            for (unsigned int i=0;i<nodos.size();i++)
                cout<<nodos[i]->dato<<endl;
            //cout<< aristas.size()<<" aristas"<<endl;
        }

        void actualizarPosNodo()
        {
            for (unsigned int i=0;i<nodos.size();i++)
                nodos[i]->pos=i;
        }

        int posNodo(TipoNodo a)
        {
            nodoG *p;
            if (buscar_nodo(a,p))
                return p->pos;
            return -1;
        }

        int posNodo2(int n, int a, int &peso)
        {
            aristaG *p=nodos[n]->aristas[a];
            peso=p->dato;
            if (p->nodos[0]==nodos[n])
                return p->nodos[1]->pos;
            return p->nodos[0]->pos;
        }

    protected:
    private:

        bool buscar_nodo(TipoNodo a,nodoG *&p)
        {
            unsigned int i;
            for (i=0 ; i<nodos.size() and !(nodos[i]->comprobar(a)) ; i++){}
            if (i<nodos.size() and nodos[i]->comprobar(a))
            {
                p=nodos[i];
                return true;
            }
            return false;
        }

        bool buscar_arista(nodoG *&p, nodoG *&q)
        {
            unsigned int i;

            for (i=0 ; i<aristas.size() and !(aristas[i]->comprobar(p,q)) ; i++){}
            if (i<aristas.size() and aristas[i]->comprobar(p,q)) return true;
            return false;
        }

        void insertionSortNodo(nodoG *a)
        {
            nodos.push_back(a);
            int j=nodos.size()-1;
            while (j>0 and nodos[j]->dato < nodos[j-1]->dato)
            {
                swap(nodos[j],nodos[j-1]);
                j--;
            }
        }
};



#endif // GRAFO_H
