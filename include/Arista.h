#ifndef ARISTA_H
#define ARISTA_H
#include "grafo.h"
#include "vector"

using namespace std;


template <typename G>
class Arista
{
    public:
        public:
        typedef typename G::TipoArista A;
        typedef typename G::nodoG N;

        vector<N*> nodos;
        int direccion;
        A dato;

        Arista(N* a,N* b, A d)
        {
            dato=d;
            nodos.push_back(a);
            nodos.push_back(b);
        }
        virtual ~Arista() {}

        bool comprobar(N *a,N *b)
        {
            if (nodos[0]==a and nodos[1]==b) return true;
            return false;
        }

        void limpiar(){nodos.clear();}

    protected:
    private:
};

#endif // ARISTA_H
