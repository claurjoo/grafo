#include <iostream>
#include "grafo.h"
#include <fstream>
#include "string"
#include "karger.h"
#include "Disktra.h"
#include <sstream>

using namespace std;

int conv_str_num(string a)
{
    istringstream convert(a);
    int res;
    if ( !(convert >> res) )
        res = 0;
    return res;
}

int main()
{

    Grafo<int,int> grafito;
    Disktra<int,int> d;

    char cadena[512];
    string cad;
    ifstream archivo("dijkstraData.txt");
    int a,b,c;

    try{
        while(!archivo.eof()) {
            archivo.getline(cadena, 512);
            unsigned int i=0,j=0;
            cad=cadena;
            while (i<cad.size())
            {
                if (cad[i]=='\t')
                {
                    if (j==0) a=conv_str_num(cad.substr(j,i-j));
                    else
                    {
                        c=conv_str_num(cad.substr(j,i-j));
                        grafito.insertar(a,b,c);
                    }
                    j=i+1;
                }else if(cad[i]==',')
                {
                    b=conv_str_num(cad.substr(j,i-j));
                    j=i+1;
                }
                i+=1;
            }
        }
        archivo.close();
    }catch(int e){
        return false;
        archivo.close();
    }
    cout<<"listo! grafo creado"<<endl<<endl;

    d.generarArbol(grafito,1);
    d.buscar(7,grafito);
    d.buscar(37,grafito);
    d.buscar(59,grafito);
    d.buscar(82,grafito);
    d.buscar(99,grafito);
    d.buscar(115,grafito);
    d.buscar(133,grafito);
    d.buscar(165,grafito);
    d.buscar(188,grafito);
    d.buscar(197,grafito);
    //int nume=1/2;
    //cout<<nume<<endl;
    cout<<endl;
    return 0;
}
